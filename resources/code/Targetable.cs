using UnityEngine;
using System.Collections;

public class Targetable : MonoBehaviour {

	protected double health = 100;
	protected double maxHealth = 100;

	public bool IsDead()
	{
		return health <= 0;
	}

	public void Damage(double damage)
	{
		health -= damage;
		if (IsDead())
		{
			Destroy(this.gameObject);
			Destroy(this);
		}
	}
}

