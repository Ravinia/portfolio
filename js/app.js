angular.module("app", ['ngRoute', 'worksService'])
.config(function($routeProvider)
{
	$routeProvider.
	when('/', {
		templateUrl: 'partials/home.html'
	}).
	when('/resume', {
		templateUrl: 'partials/resume.html'
	}).
	when('/works', {
		templateUrl: 'partials/works.html',
		controller: 'worksController'
	}).
	when('/gallery', {
		templateUrl: 'partials/gallery.html'
	}).
	when('/code', {
		templateUrl: 'partials/code.html'
	}).
	otherwise({
		redirectTo: '/'
	});
})
.filter('linkFilter', function()
{
	return function(input)
	{
		//console.log('input: ' + JSON.stringify(input))
		var out = {};
		angular.forEach(input, function(value, key)
		{
			//console.log('key: ' + key);
			//console.log('value: ' + value);
			if (key != 'Link' && key != 'Name')
			{
				out[key] = value;
			}
		})
		return out;
	}
})
.controller('worksController', ['$scope', 'worksJSON', function($scope, worksJSON)
{
	$scope.works = worksJSON
}]);
